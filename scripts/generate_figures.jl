#<<
using R1R4
using Plots
using Latexify
using Measurements
using CSV
using DataFrames
using Clustering
using Statistics
Latexify.copy_to_clipboard(true)

using RecipesBase
@recipe function f(x::AbstractArray, y::AbstractArray{<:Measurements.Measurement})
    ribbon := Measurements.uncertainty.(y)
    x, Measurements.value.(y)
end

#>>
#==============================================================================#
#============================  Script parameters  =============================# 
#==============================================================================#
#<<

use_uncertainty = true
use_stochasticity = false # not to be used in combination with uncertainty. Only applies to figure 2.
save_plots = true

if use_uncertainty
    savename1 = "exp_vs_imp_uncertainty"
    savename2 = "feedback_bio_param_uncertainty"
elseif use_stochasticity
    savename1 = "exp_vs_imp"
    savename2 = "feedback_bio_param_stochastic"
else
    savename1 = "exp_vs_imp"
    savename2 = "feedback_bio_param"
end

#==============================================================================#
#=======================  Assumptions and measurements  =======================# 
#==============================================================================#

#===================  Measured nuclear to cytosolic ratio  ====================# 
data = DataFrame(CSV.File("data/MYB3R4.csv"))

## n_tot - measured total nuclear intensity
n(data) = data.n_tot

## w_tot - measured total whole-cell intensity
tot(data) = data.w_tot

## (w_tot - n_tot) - inferred cytosolic intensity.
c(data) = data.w_tot .- data.n_tot

## Separate high - low nuclear localisation data.
cluster_result = kmeans(permutedims([n(data) ./ tot(data)   c(data) ./ tot(data)]), 2)

## The largest cluster has a low ratio
data1 = data[cluster_result.assignments .== findmax(cluster_result.counts)[2], :]
## The smallest cluster has a high ratio
data2 = data[cluster_result.assignments .== findmin(cluster_result.counts)[2], :]

scatter(c(data1), n(data1), label="II")
scatter!(c(data2), n(data2), label="I")

#>>
#===============================  Assumptions  ================================# 
#<<
if use_uncertainty
    ## CYT peak height ~400
    γ = measurement(2500., 1000.)

    ## Nuclear to cytosolic MYB3R4 ratio pre-CYT.
    Δ = measurement(mean(n(data1) ./ c(data1)), std(n(data1) ./ c(data1)))

    ## Nuclear to cytosolic MYB3R4 localisation rate without CYT and IMPA3/IMPA6
    t_eq = measurement(6. / 60., 3. / 60) 
else
    γ = 2500.
    Δ = mean(n(data1) ./ c(data1))
    t_eq = 6. / 60.
end

cyt_application_time = 1.
tspan = (0., 4)
trange = range(0.001, stop = tspan[end], length = 301)
trigger = InfusionTrigger(cyt_application_time, γ, :X_1)

#==============================================================================#
#=================  Functions to calculate parameter values  ==================# 
#==============================================================================#

import_rate(::ModelCytExport, t_eq, Δ; k = 10.) = log(2) / (t_eq * (1 + 1/Δ))
export_rate(::ModelCytExport, t_eq, Δ; k = 10.) = log(2) * k / (t_eq * (1 + Δ))

import_rate(::ModelCytImport, t_eq, Δ; k = 10.) = log(2) / (t_eq * k * (1 + 1/Δ))
export_rate(::ModelCytImport, t_eq, Δ; k = 10.) = log(2) / (t_eq * (1 + Δ))


hill(x, v, k, n) = v * x^n / (k^n + x^n)
β(ratio, p) = hill(ratio * p[:R4_tot] / (1 + ratio), p[:v_i], p[:k_n_imp], p[:n_hill])
latexify(:(β = v * (Δ * R4_tot / (1 + Δ))^n / (α^n + (Δ * R4_tot / (1 + Δ))^n)) )

import_rate(::ModelCytFeedback, t_eq, Δ, p) = (log(2)/t_eq - β(Δ, p)/Δ) / (1 + 1/Δ)
export_rate(::ModelCytFeedback, t_eq, Δ, p) = (log(2)/t_eq + β(Δ, p)) * (p[:k] + p[:eq_cyt]) / (1 + Δ)

import_rate(::ModelCytImportFeedback, t_eq, Δ, p) = (log(2)/(t_eq * (p[:k] + p[:eq_cyt]))  - β(Δ, p)/Δ) / (1 + 1/Δ)
export_rate(::ModelCytImportFeedback, t_eq, Δ, p) = (log(2)/t_eq + β(Δ, p) * (p[:k] + p[:eq_cyt])) / (1 + Δ)


#==============================================================================#
#====================  Model and parameter specification  =====================# 
#==============================================================================#


#===============================  Import model  ===============================# 
model_i = ModelCytImport()
p_i = [10., 10., import_rate(model_i, t_eq, Δ; k=100.), 100., export_rate(model_i, t_eq, Δ; k=100.), 1000., 0]
p_i = Param(model_i, p_i)

#===============================  Export model  ===============================# 
model_e = ModelCytExport()
p_e = [10., 10., import_rate(model_e, t_eq, Δ; k=100.), 100., export_rate(model_e, t_eq, Δ; k=100.), 1000., 0]
p_e = Param(model_e, p_e)


#=========================  Import + feedback model  ==========================# 
model_i_fb = ModelCytImportFeedback()
p_i_fb = [10., 10., 0.1, import_rate(model_i, t_eq, Δ; k=100.), 1000, 100., export_rate(model_i, t_eq, Δ; k=100.), 0., 1000., 1]
p_i_fb = Param(model_i_fb, p_i_fb)
p_i_fb[:r_i] = import_rate(model_i_fb, t_eq, Δ, p_i_fb)
p_i_fb[:r_e] = export_rate(model_i_fb, t_eq, Δ, p_i_fb)
p_i_fb


#=========================  Export + feedback model  ==========================# 
model_e_fb = ModelCytFeedback()
p_e_fb = [10., 10., 10., import_rate(model_e, t_eq, Δ; k=100.), 1000, 100., export_rate(model_e, t_eq, Δ; k=100.), 0., 1000, 1]
p_e_fb = Param(model_e_fb, p_e_fb)
p_e_fb[:r_i] = import_rate(model_e_fb, t_eq, Δ, p_e_fb)
p_e_fb[:r_e] = export_rate(model_e_fb, t_eq, Δ, p_e_fb)
p_e_fb
#>>

#==============================================================================#
#========================  LaTeXify parameter tables  =========================# 
#==============================================================================#

using LaTeXStrings

## non-feedback models
l= latexify([p_i.model.params p_i[:] p_e[:]], env=:table, head=LaTeXString.(["Parameter", "Import model", "Export model"])) 
l = LaTeXString(replace(l, "± 0.0\$"=> "\$"))
l = LaTeXString(replace(l, "±"=> raw"\pm"))
clipboard(l)
# render(l)

## feedback models
l= latexify([p_i_fb.model.params p_i_fb[:] p_e_fb[:]], env=:table, head=LaTeXString.(["Parameter", "Import model", "Export model"])) 
l = LaTeXString(replace(l, "± 0.0\$"=> "\$"))
l = LaTeXString(replace(l, "±"=> raw"\pm"))
clipboard(l)
# render(l)

#==============================================================================#
#================  Simulations and plotting - Model figure 1  =================# 
#==============================================================================#
#<<

plot(layout = (3,2), legend = false, grid = false, fontfamily = "Times", titleloc = :left, size = (700,400));
plot_attr = Dict(:linewidth =>2, :fillalpha =>0.3)

#===============================  Trajectories  ===============================# 

### export model
sim = simulate(ODE(), trigger, model_e, p_e; tspan = tspan)
f(t) = sim(t)[7] / (p_e[:R4_tot] - sim(t)[7])
if f(1.0) isa Measurement
    plot!(
        subplot = 2,
        trange,
        Measurements.value.(sim(trange; idxs = 6)); 
        ribbon = [Measurements.uncertainty.(sim.(trange; idxs = 6))],
        label = "Export model",
        plot_attr...,
    );
    plot!(
        subplot = 4,
        trange,
        Measurements.value.(sim(trange; idxs = 7)); 
        ribbon = [Measurements.uncertainty.(sim.(trange; idxs = 7))],
        label = "Export model",
        plot_attr...,
    );
    plot!(
        subplot = 6,
        trange,
        Measurements.value.(f.(trange)); 
        ribbon = [Measurements.uncertainty.(f.(trange))],
        fillalpha = 0.2,
        plot_attr...,
    );
else
    plot!(subplot = [2 4], sim; vars = 6:7, label = "Export model", plot_attr...);
    plot!(subplot = 6, trange, f.(trange);plot_attr...);
end;


### import model
sim = simulate(ODE(), trigger, model_i, p_i; tspan = tspan)
f(t) = sim(t)[7] / (p_i[:R4_tot] - sim(t)[7])
if use_uncertainty
    plot!(
        subplot = 2,
        trange,
        Measurements.value.(sim(trange; idxs = 6)); 
        ribbon = [Measurements.uncertainty.(sim.(trange; idxs = 6))],
        label = "Import model",
        plot_attr...,
    )
    plot!(
        subplot = 4,
        trange,
        Measurements.value.(sim(trange; idxs = 7)); 
        ribbon = [Measurements.uncertainty.(sim.(trange; idxs = 7))],
        label = "Import model",
        plot_attr...,
    )
    plot!(
        subplot = 6,
        trange,
        Measurements.value.(f.(trange)); 
        ribbon = [Measurements.uncertainty.(f.(trange))],
        fillalpha = 0.2,
        plot_attr...,
    )
else
    plot!(subplot = [2 4], sim; vars = 6:7, label = "Import model", plot_attr...);
    plot!(subplot = 6, trange, f.(trange);plot_attr...);
end

## Target peak value
hline!(subplot=6, [mean(n(data2) ./ c(data2))], ribbon = std(n(data2) ./ c(data2)); color=:black, linealpha=0.4, fillalpha=0.1)

## Formatting
foreach(i -> plot!(subplot = 2i, yguide = ["CYT" "N" "N/C"][i], xguide = ""), 1:3)
plot!(subplot = 6, xguide = "Time [h]");
plot!(subplot = 2, legend = (0.65,0.9), fg_legend = :transparent, bg_legend = :transparent)



#============================  Equilibration time  ============================# 

t_half_exp(cyt, p) = log(2) * (p[:k] + cyt) / (p[:r_e] + p[:r_i] * (p[:k] + cyt))
t_half_imp(cyt, p) = log(2) / (p[:r_e] + p[:r_i] * (p[:k] + cyt))

subplot = 3
max_cyt = 500.
cytrange = range(0., stop = max_cyt, length = 401)
if t_eq isa Measurement
plot!(
    subplot = subplot,
    cytrange,
    Measurements.value.((cyt -> t_half_exp(cyt, p_e) * 60).(cytrange));
    label = "Export model",
    ribbon = Measurements.uncertainty.((cyt -> t_half_exp(cyt, p_e) * 60).(cytrange)),
    plot_attr...,
);

plot!(
      subplot = subplot,
      cytrange,
      Measurements.value.((cyt -> t_half_imp(cyt, p_i) * 60).(cytrange));
      label = "Import model",
      ribbon = Measurements.uncertainty.((cyt -> t_half_imp(cyt, p_i) * 60).(cytrange)),
      plot_attr...,
     );
else
    plot!(subplot = subplot, cyt -> t_half_exp(cyt, p_e) * 60, cytrange; label = "Export model",  plot_attr...);
    plot!(subplot = subplot, cyt -> t_half_imp(cyt, p_i) * 60, cytrange; label = "Import model", plot_attr...);
end
plot!(subplot = subplot, xguide = "CYT", yguide = Latexify.L"t_{eq} \mathrm{[min]}", ylims = (0., Inf));


subplot = 5
eq(cyt, p) = p[:r_i] * (p[:k] + cyt) * p[:R4_tot] / (p[:r_i] * (p[:k] + cyt) + p[:r_e])


if t_eq isa Measurement
    plot!(
        subplot = subplot,
        cytrange,
        Measurements.value.((cyt -> eq(cyt, p_e)).(cytrange));
        label = "Export model",
        ribbon = Measurements.uncertainty.((cyt -> eq(cyt, p_e)).(cytrange)),
        plot_attr...,
   );
    plot!(
        subplot = subplot,
        cytrange,
        Measurements.value.((cyt -> eq(cyt, p_i)).(cytrange));
        label = "Import model",
        ribbon = Measurements.uncertainty.((cyt -> eq(cyt, p_i)).(cytrange)),
        style=:dash, 
        plot_attr...,
   );
else
    plot!(subplot = subplot, cyt -> eq(cyt, p_e), cytrange; label = "Export model",  plot_attr...);
    plot!(subplot = subplot, cyt -> eq(cyt, p_i), cytrange; label = "Import model", style = :dash,  plot_attr...);
end

plot!(subplot = subplot, xguide = "CYT", yguide = Latexify.L"N_{eq}");
plot!(title = permutedims(["$(('a':'z')[i]))" for i in eachindex(plot!().subplots)]))

scatter!(subplot=1, c(data2), n(data2), label="I", ms=2, color=4)
scatter!(
    subplot=1,
    c(data1),
    n(data1),
    grid = false,
    label="II",
    xguide="C",
    yguide="N",
    color=3,
    ms=2,
    markerstrokecolor=:match,
    legend=(0.9,1.0),
    fg_legend=:transparent,
    bg_legend=:transparent,
);

plot!()

#>>

#===============================  Save figure  ================================# 

plot!(dpi = 600);
save_plots && foreach(fmt -> savefig("results/$(savename1).$fmt"), [:png, :pdf, :svg])
plot!(dpi = 90)

#==============================================================================#
#==================  Simulate and plot - modelling figure 2  ==================# 
#==============================================================================#


#<<
l = @layout [grid(3,2){0.7w} grid(2,1)]
plot(layout = l, legend=false, grid=false, fontfamily = "Times", titleloc=:left, link=:y, size=(700, 400));

if use_stochasticity
    plot_attr = Dict(:yguide=>"", :linealpha=>0.2, :linewidth=>1)
else
    plot_attr = Dict(:yguide=>"", :linealpha=>1.0, :linewidth=>3)
end

colors = (exp=3, imp=3, exp_fb=4, imp_fb=4)

subplots = Dict(
                :t_e_cyt => 1,
                :t_e_n => 3,
                :t_e_nc => 5,
                :t_i_cyt => 2,
                :t_i_n => 4,
                :t_i_nc => 6,
                :dndt_i => 8,
                :dndt_e => 7,
               )

#===============================  Trajectories  ===============================# 

simulator = use_stochasticity ? Ensemble() : ODE()

### export model
sim = simulate(simulator, trigger, model_e, p_e; tspan = tspan)
plot!(
      subplot=[subplots[:t_e_cyt] subplots[:t_e_n]],
      sim; vars=6:7,
      # label="No feedback",
      label="",
      color = colors[:exp],
      plot_attr...,
     );
if simulator isa Ensemble
    for sol in sim
        plot!(
              subplot=subplots[:t_e_nc],
              t -> sol(t)[7] / (p_e[:R4_tot] - sol(t)[7]),
              range(0.001, stop=tspan[end], length=401); 
              color = colors[:exp],
              plot_attr...,
             );
    end
else
    plot!(
          subplot=subplots[:t_e_nc],
          trange,
          (t -> sim(t)[7] / (p_e[:R4_tot] - sim(t)[7])).(trange);
          color = colors[:exp],
          plot_attr...,
         );
end


## Feedback export model
sim = simulate(simulator, trigger, model_e_fb, p_e_fb; tspan = tspan)
plot!(
      subplot=[subplots[:t_e_cyt] subplots[:t_e_n]],
      sim; 
      vars=6:7,
      color = colors[:exp_fb],
      # label="Feedback",
      label="",
      plot_attr...,
     );
if simulator isa Ensemble
    for sol in sim
        plot!(
              subplot=subplots[:t_e_nc],
              trange,
              (t -> sol(t)[7] / (p_e_fb[:R4_tot] - sol(t)[7])).(trange);
              color = colors[:exp_fb],
              plot_attr...,
             );
    end
else
    plot!(
          subplot=subplots[:t_e_nc],
          trange,
          (t -> sim(t)[7] / (p_e_fb[:R4_tot] - sim(t)[7])).(trange);
          color = colors[:exp_fb],
          plot_attr...,
         );
end


### import model
sim = simulate(simulator, trigger, model_i, p_i; tspan = tspan)
plot!(
    subplot=[subplots[:t_i_cyt] subplots[:t_i_n]],
    sim; vars=6:7,
    color = colors[:imp],
    # label="Import model",
    label="",
    plot_attr...,
);

if simulator isa Ensemble
    for sol in sim
        plot!(
              subplot=subplots[:t_i_nc],
              t -> sol(t)[7] / (p_i[:R4_tot] - sol(t)[7]),
              range(0.001, stop=tspan[end], length=401); 
              color = colors[:imp],
              plot_attr...,
             );
    end
else
    plot!(
          subplot=subplots[:t_i_nc],
          trange,
          (t -> sim(t)[7] / (p_i[:R4_tot] - sim(t)[7])).(trange);
          color = colors[:imp],
          plot_attr...,
         );
end


### import feedback model
sim = simulate(simulator, trigger, model_i_fb, p_i_fb; tspan = tspan)
plot!(
      subplot=[subplots[:t_i_cyt] subplots[:t_i_n]],
      sim; vars=6:7,
      # label="Import feedback model",
      label="",
      color = colors[:imp_fb],
      plot_attr...,
     );
if simulator isa Ensemble
    for sol in sim
        plot!(
              subplot=subplots[:t_i_nc],
              t -> sol(t)[7] / (p_i_fb[:R4_tot] - sol(t)[7]),
              range(0.001, stop=tspan[end], length=401); 
              color = colors[:imp_fb],
              plot_attr...,
             );
    end
else
    plot!(
          subplot=subplots[:t_i_nc],
          trange,
          (t -> sim(t)[7] / (p_i_fb[:R4_tot] - sim(t)[7])).(trange);
          color = colors[:imp_fb],
          plot_attr...,
         );
end

hline!(subplot=subplots[:t_i_nc], [mean(n(data2) ./ c(data2))], ribbon = std(n(data2) ./ c(data2)); color=:black, linealpha=0.4, fillalpha=0.1);
hline!(subplot=subplots[:t_e_nc], [mean(n(data2) ./ c(data2))], ribbon = std(n(data2) ./ c(data2)); color=:black, linealpha=0.4, fillalpha=0.1);

plot!(subplot=subplots[:t_e_cyt], legend = :topright);

plot!(subplot=6, xguide="Time [h]");

#============================  Derivatives plots  =============================# 

dndt(model::Union{ModelCytExport, ModelCytImport}, n, p, cyt) = model([0., 0., 0., 0., 0., cyt, n], p, 0.)[7]
dndt(model::Union{ModelCytFeedback, ModelCytImportFeedback}, n, p, cyt) = model([0., 0., 0., 0., 0., cyt, n], p, 0.)[7]

cyt_peak_value = p_i[:R4_tot] isa Measurement ? measurement(400., 0.) : 400.

subplot = subplots[:dndt_e]
nrange = p_i[:R4_tot] isa Measurement ? range(0., stop=Measurements.value(p_i[:R4_tot]), length=300) : range(0., stop=p_i[:R4_tot], length=300)
plot!(
    subplot=subplot,
    nrange,
    (n -> dndt(model_e, n, p_e, p_e[:eq_cyt])).(nrange);
    color=colors[:exp],
    label="",
    lw = plot_attr[:linewidth],
);
plot!(
    subplot=subplot,
    nrange,
    (n -> dndt(model_e_fb, n, p_e_fb, p_e_fb[:eq_cyt])).(nrange);
    color=colors[:exp_fb],
    label="",
    lw = plot_attr[:linewidth],
);

hline!(subplot=subplot, [0.], label="", color=:black, alpha=0.4, lw=3);
plot!(
    subplot=subplot,
    nrange,
    (n -> dndt(model_e, n, p_e, cyt_peak_value)).(nrange);
    color=colors[:exp],
    label="",
    lw = plot_attr[:linewidth],
    style=:dash,
);
plot!(
    subplot=subplot,
    nrange,
    (n -> dndt(model_e_fb, n, p_e_fb, cyt_peak_value)).(nrange);
    color=colors[:exp_fb],
    label="",
    lw = plot_attr[:linewidth],
    style=:dash,
);
plot!(subplot=subplot, xguide="N", yguide="dN/dt");
plot!()


subplot = subplots[:dndt_i]
plot!(
    subplot=subplot,
    nrange,
    (n -> dndt(model_i, n, p_i, p_i[:eq_cyt])).(nrange);
    color=colors[:imp],
    # label="No feedback",
    label="",
    lw = plot_attr[:linewidth],
);
plot!(
    subplot=subplot,
    nrange,
    (n -> dndt(model_i_fb, n, p_i_fb, p_i_fb[:eq_cyt])).(nrange);
    color=colors[:imp_fb],
    # label="Feedback",
    label="",
    lw = plot_attr[:linewidth],
);

hline!(subplot=subplot, [0.], label="", color=:black, alpha=0.4, lw=3);
plot!(
    subplot=subplot,
    nrange,
    (n -> dndt(model_i, n, p_i, cyt_peak_value)).(nrange);
    color=colors[:imp],
    # label="No feedback",
    label="",
    lw = plot_attr[:linewidth],
    style=:dash,
);
plot!(
    subplot=subplot,
    nrange,
    (n -> dndt(model_i_fb, n, p_i_fb, cyt_peak_value)).(nrange);
    color=colors[:imp_fb],
    # label="Feedback",
    label="",
    lw = plot_attr[:linewidth],
    style=:dash,
);
plot!(subplot=subplot, xguide="N", yguide="dN/dt");

#==============================  Final touches  ===============================# 

plot!(title = permutedims(["$(('a':'z')[i]))" for i in eachindex(plot!().subplots)]));

plot!(title_style="{at = {(0,1)}, anchor = south west}", extra_kwargs = :subplot);
plot!(subplot=subplots[:t_e_cyt], title = "a) Export model");
plot!(subplot=subplots[:t_i_cyt], title = "b) Import model");
plot!(subplot=subplots[:dndt_e], title = "g) Export model");
plot!(subplot=subplots[:dndt_i], title = "h) Import model");

plot!(subplot=subplots[:t_i_cyt], yguide="CYT", ylims=(0., 5.), xguide="");
plot!(subplot=subplots[:t_e_cyt], yguide="CYT", ylims=(0., 5.), xguide="");

plot!(subplot=subplots[:t_i_n], yguide="N", ylims=(0., 1.), xguide="");
plot!(subplot=subplots[:t_e_n], yguide="N", ylims=(0., 1.), xguide="");

plot!(subplot=subplots[:t_i_nc], yguide="N/C", ylims=(0., 5.5), xguide="Time [h]");
plot!(subplot=subplots[:t_e_nc], yguide="N/C", ylims=(0., 5.5), xguide="Time [h]");

plot!(ylims=(0., Inf));

plot!(legend=false);


plot!(subplot=2, [],[], color=colors[:exp], label="No feedback");
plot!(subplot=2, [],[], color=colors[:exp_fb], label="Feedback");
plot!(subplot=2, [],[], color=:grey, label="Target peak");
plot!(subplot=2, legend=(0.8, 0.9), fg_legend=:transparent, bg_legend=:transparent);
plot!(subplot=7, [],[], color=:black, label="No CYT");
plot!(subplot=7, [],[], color=:black, style=:dash, label="CYT");
plot!(subplot=7, legend=(0.35, 0.3), fg_legend=:transparent, bg_legend=:transparent);

plot!(subplot=7, ylims=(-Inf, Inf));
plot!(subplot=8, ylims=(-Inf, Inf))

#>>
#<<
#===============================  Save figure  ================================# 
plot!(dpi = 600);
save_plots && foreach(fmt -> savefig("results/$savename2.$fmt"), [:png, :pdf, :svg])
plot!(dpi = 90)

#>>
