using R1R4
using Plots
using Latexify

##
pgfplotsx()

##

model = ModelCytExport()
# model = ModelCytImport()
p = Param(model)

tcyt = 4.
cyt = 550.
tspan = (0., 10.)
trange = range(eps(), stop=tspan[end], length=400)
u0 = fill(0., length(model.syms))
N_ind = findfirst(==(:N), model.syms)

##

sim = simulate(ODE(), InfusionTrigger(tcyt, cyt, :X_1), model, p; tspan=tspan, u0=u0)
plot(layout = (2,2), legend=false, xlims=tspan);
plotvars = 6:7
plotattr = Dict(:linewidth => 3, :alpha => 1., :color => 1)
plot!(subplot=1:length(plotvars), sim; vars = plotvars, yguide = permutedims(string.(model.syms[plotvars])), plotattr...)

total_r4(t, sim, model::Union{ModelCytExport, ModelCytImport}, p) = p[:R4_tot]
cytosolic_r4(t, sim, model::Union{ModelCytExport, ModelCytImport}, p) = p[:R4_tot] - sim(t)[7]
plot!(subplot=3, t->sim(t)[N_ind]/cytosolic_r4(t, sim, model, p), trange; yguide="N/C", plotattr...)
plot!(subplot=4, t->sim(t)[N_ind]/total_r4(t, sim, model, p), trange; yguide="N/Total", plotattr..., ylims=(0., 1.))

plot!(xguide="Time [hours]")


##
foreach(fmt -> Plots.savefig("results/simple_export.$fmt"), [:png, :pdf, :svg])

##
revise()

dndt(model::Union{ModelCytExport, ModelCytImport}, n, p) = model([0., 0., 0., 0., 0., p[:eq_cyt], n], p, 0.)[7]
plot(n -> dndt(model, n, p), range(0., stop=p[:R4_tot], length=300), label="No feedback")
hline!([0.], label="", color=:black, alpha=0.4, lw=3)


model_fb = ModelCytFeedback()
dndt(model::ModelCytFeedback, n, p) = model([0., 0., 0., 0., 0., p[:eq_cyt], n], p, 0.)[7]
p_fb = Param(model_fb)
p_fb[:v_i] = p_fb[:r_i] * 5.
foreach(key -> p_fb[key] = p[key], model.params)
plot!(n -> dndt(model_fb, n, p_fb), range(0., stop=p[:R4_tot], length=300), label="Feedback")
