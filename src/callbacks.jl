@inline cyt_start() = 3
@inline cyt_end() = 4
cyt_condition(u, t, i) =  - (cyt_start() - t) * (cyt_end() - t)

function cyt_affect!(i) 
    if isapprox(i.t, cyt_start(); rtol=1e-3, atol=1e-3)
        i.p[:r_export] /= 10
        #= i.p[:r_import] *= 10 =#
    elseif isapprox(i.t, cyt_end(); rtol=1e-3, atol=1e-3)
        i.p[:r_export] *= 10
        #= i.p[:r_import] /= 10 =#
    end
    #= @show i.p =#
end

cyt_callback = ContinuousCallback(cyt_condition, cyt_affect!; save_positions=(true, true))

