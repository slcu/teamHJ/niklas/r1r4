#### A function to interactively explore model parameters by using sliders.

interactive_plot(model; kwargs...) = interactive_plot(model, fill(1., length(model.params)); kwargs...)
function interactive_plot(model, param; use_trigger=true, trigger_param=:r_i)
    p = [slider(vcat(0., 10. .^ range(-4,stop=4, length=150)), label=model.params[i], value=param[i]) for i in 1:length(model.params)]
    for i in 1:length(p)
        if first(string(params(model)[i])) == 'n'
            p[i] = slider(1.:30., label=params(model)[i], value = param[i])
        end
    end
    
    trigger_params = [
        slider(10. .^ range(0,stop=3, length=150), label="Tstop", value=10.),
        slider(10. .^ range(-1,stop=2, length=150), label="Trigger start", value=2.),
        slider(10. .^ range(-1,stop=2, length=150), label="Trigger duration", value=1.),
        slider(10. .^ range(-4,stop=4, length=150), label="Trigger value", value=10.),
    ]
    
    num_stochastic = slider([0, 1, 3, 5, 10, 25, 50, 100], label="Num stochastic", value=0)
    repeat = button("Repeat")
    
    plt = map(throttle.(1., p)..., throttle.(1., trigger_params)..., throttle(1., num_stochastic), repeat) do vals...
        
        param .= vals[1:length(param)]
        tstop, ttrig, trig_duration, trig_value = vals[length(param)+1:length(param)+4]
        num_repeats = vals[length(param)+5]
        trigger = ImportTrigger((ttrig, ttrig+trig_duration), trig_value, trigger_param)
        
        plot(
            simulate(ODE(), NoTrigger(), model, param; tspan=(0., tstop));
            label=["Cytosolic" "Nuclear"], 
            vars = 1:2,
            color = 2,
            lw = 3,
            linestyle = [:dash :solid],
        )
        
        if use_trigger
            plot!(
                simulate(ODE(), trigger, model, param; tspan=(0., tstop));
                label=["C - trigger" "N - trigger"], 
                vars=1:2,
                color = 1,
            lw = 3,
                linestyle = [:dash :solid]
            )
        end
        
        if num_repeats >=1 
            plot!(
                simulate(Ensemble(), NoTrigger(), model, param; trajectories=num_repeats, tspan=(0., tstop));
                alpha=0.4,
                color=2, 
                vars=2,
            lw = 3,
                label="",
            )

            if use_trigger
                plot!(
                    simulate(Ensemble(), trigger, model, param; trajectories=num_repeats, tspan=(0., tstop));
                    alpha=0.4,
                    color=1, 
                    vars=2,
            lw = 3,
                    label="",
                )
            end
        end
        
        plot!(legend=true)
    end

    hbox(vbox(repeat,num_stochastic, p..., trigger_params...), plt)
end

