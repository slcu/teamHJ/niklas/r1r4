module R1R4

using Reexport
@reexport using DiffEqParameters
using DifferentialEquations
using DiffEqBiological
using DiffEqSensitivity
using Latexify
using DiffEqBase
using RecipesBase
using Interact
using Plots


abstract type AbstractSimulation end
struct ODE <: AbstractSimulation end
struct Gillespie <: AbstractSimulation end
struct Ensemble <: AbstractSimulation end

# abstract type AbstractTrigger end 
# struct NoTrigger <: AbstractTrigger end
# struct Trigger <: AbstractTrigger 
#     times::Tuple{Float64, Float64}
#     value::Float64
# end
# Trigger() = Trigger((2., 3.), 10.)

include("models.jl")
export R4Feedback, R4FeedbackCYT, ModelCytImport, ModelCytExport, ModelCytFeedback, ModelCytProdFeedback, ModelCytImportFeedback

include("triggers.jl")
export AbstractTrigger, NoTrigger, ImportTrigger, ExportTrigger, InfusionTrigger

include("simulate.jl")
# include("callbacks.jl")
export simulate, add_output!, ODE, Gillespie, Ensemble

include("plot_recipes.jl")
include("interaction.jl")
export interactive_plot

end # module
